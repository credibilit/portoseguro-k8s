# portoseguro-k8s

![Alt text](https://bitbucket.org/credibilit/portoseguro-k8s/raw/9592b7776850431847900e09cf4ad819acd04c28/docs/kubernetes.jpeg)

export output terraform to user in kops:

    TF_OUTPUT=$(terraform output -json)
    echo ${TF_OUTPUT} | jq -r .s3_endpoint
    echo ${TF_OUTPUT} | jq -r .kops_s3_bucket.value.name
    CLUSTER_NAME="$(echo ${TF_OUTPUT} | jq -r .kops_s3_bucket.value.name)"
    STATE="s3://$(echo ${TF_OUTPUT} | jq -r .kops_s3_bucket.value.name)"