variable "account" {
  type        = "string"
  description = "The AWS account id to create the environment"
  default     = "654040296502"
}

locals {
  environment    = "prd"
  stack          = "k8s"
  region         = "saopaulo"
  vpc_cidr_block = "10.1.0.0/16"
  name           = "${local.stack}-${local.environment}"
  az_count       = 2
  comment        = "Kubernetes - PortoSeguro"
  instance_type  = "m4.large"                            #2vcpu - 8gb

  public_subnets_cidr_block = [
    "10.1.10.0/24",
    "10.1.11.0/24",
  ]

  # "10.1.12.0/24",

  private_master_subnets_cidr_block = [
    "10.1.20.0/24",
    "10.1.21.0/24",
  ]

  # "10.1.22.0/24",

  private_node_subnets_cidr_block = [
    "10.1.30.0/24",
    "10.1.31.0/24",
  ]

  # "10.1.32.0/24",

  private_database_subnets_cidr_block = [
    "10.1.40.0/24",
    "10.1.41.0/24",
  ]

  # "10.1.42.0/24",

  tags = {
    "Environment" = "${local.environment}"
  }
}

data "aws_availability_zones" "azs" {
  state = "available"
}

data "template_file" "users" {
  template = "${file("${path.module}/scripts/claranet_users.sh")}"
}
