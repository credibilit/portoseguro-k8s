module "network" {
  source = "git::https://bitbucket.org/credibilit/stack-network-k8s?ref=1.0.1"

  environment                         = "hml"
  name                                = "${local.name}"
  domain_name                         = "${local.name}.local"
  cidr_block                          = "${local.vpc_cidr_block}"
  hosted_zone_comment                 = "${local.comment}"
  tags                                = "${local.tags}"
  public_subnets_cidr_block           = "${local.public_subnets_cidr_block}"
  private_node_subnets_cidr_block     = "${local.private_node_subnets_cidr_block}"
  private_master_subnets_cidr_block   = "${local.private_master_subnets_cidr_block}"
  private_database_subnets_cidr_block = "${local.private_database_subnets_cidr_block}"
  azs                                 = "${data.aws_availability_zones.azs.names}"
  az_count                            = "${local.az_count}"
}

output "vpc" {
  value = {
    id                        = "${module.network.vpc["id"]}"
    cidr_block                = "${module.network.vpc["cidr_block"]}"
    cidr_block_bits           = "${module.network.vpc["cidr_block_bits"]}"
    private_zone              = "${module.network.vpc["private_zone"]}"
    instance_tenancy          = "${module.network.vpc["instance_tenancy"]}"
    main_route_table_id       = "${module.network.vpc["main_route_table_id"]}"
    default_network_acl_id    = "${module.network.vpc["default_network_acl_id"]}"
    default_security_group_id = "${module.network.vpc["default_security_group_id"]}"
    default_route_table_id    = "${module.network.vpc["default_route_table_id"]}"
    network_address           = "${module.network.vpc["network_address"]}"
    network_mask              = "${module.network.vpc["network_mask"]}"
  }
}

output "subnets-public-ids" {
  value = "${module.network.subnets-public}"
}

output "subnets-private-master-ids" {
  value = "${module.network.subnets-private-master}"
}

output "subnets-private-database-ids" {
  value = "${module.network.subnets-private-database}"
}

output "subnets-private-node-ids" {
  value = "${module.network.subnets-private-node}"
}

output "dhcp_options" {
  value = {
    id                   = "${module.network.dhcp_options["id"]}"
    domain_name_servers  = "${module.network.dhcp_options["domain_name_servers"]}"
    ntp_servers          = "${module.network.dhcp_options["ntp_servers"]}"
    netbios_name_servers = "${module.network.dhcp_options["netbios_name_servers"]}"
    netbios_node_type    = "${module.network.dhcp_options["netbios_node_type"]}"
  }
}

output "s3_endpoint" {
  value = {
    id          = "${module.network.s3_endpoint["id"]}"
    prefix_list = "${module.network.s3_endpoint["prefix_list"]}"
  }
}
