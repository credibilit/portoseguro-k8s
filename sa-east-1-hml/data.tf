variable "account" {
  type        = "string"
  description = "The AWS account id to create the environment"
  default     = "495770326048"
}

locals {
  environment    = "hml"
  stack          = "k8s"
  vpc_cidr_block = "10.2.0.0/16"
  name           = "${local.stack}-${local.environment}"
  az_count       = 1
  comment        = "Kubernetes hml - PortoSeguro"

  public_subnets_cidr_block = [
    "10.2.10.0/24",
  ]

  private_master_subnets_cidr_block = [
    "10.2.20.0/24",
  ]

  private_node_subnets_cidr_block = [
    "10.2.30.0/24",
  ]

  private_database_subnets_cidr_block = [
    "10.2.40.0/24",
  ]

  tags = {
    "Environment" = "${local.environment}"
  }
}

data "aws_availability_zones" "azs" {
  state = "available"
}

data "template_file" "users" {
  template = "${file("${path.module}/scripts/claranet_users.sh")}"
}
