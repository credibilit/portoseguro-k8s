resource "aws_s3_bucket" "kops_state" {
  bucket        = "${local.name}.kops"
  acl           = "private"
  force_destroy = true
  tags          = "${merge(local.tags)}"
}

output "kops_s3_bucket" {
  value {
    name = "${aws_s3_bucket.kops_state.bucket}"
    arn  = "${aws_s3_bucket.kops_state.arn}"
  }
}
