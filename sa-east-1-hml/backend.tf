terraform {
  required_version = ">=0.10.8"

  backend "s3" {
    bucket = "495770326048-tfstate-k8s"
    key    = "terraform/k8s-sao-hml.tfstate"
    region = "sa-east-1"
  }
}
